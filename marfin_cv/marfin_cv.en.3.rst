 
 
.. role:: raw-tex(raw)
    :format: latex html



.. default-role:: raw-tex



.. reStructuredText source for my english curriculum vitae.
..
.. The idea of writing my curriculum vitae using reStructuredText comes
.. from Martin Fellix Krafft, see <http://martin-krafft.net/cv/>.
..
.. The files making up this curriculum vitae are greatly inspired from
.. Martin Fellix Krafft's work.
..
.. This file is provided under the terms of the Artistic License 2.0.

.. |today| replace:: 5th September 2013





===============================================================================
                                Curriculum Vitae
===============================================================================




Igor Marfin, physicist
============================================

+------------------------------------+------------------------+--------------------+
|:Address: Germany (Zeuthen)         |:Born: 1979             |.. image ::   id.png|
|:Tel: +49 (0)1758916099             |:Nationality: Belarus   |   :scale: 50       |
|:Email: marfin@cern.ch              |                        |   :align: right    |
+------------------------------------+------------------------+--------------------+



-------------------------------------------------------------------------------
Education
-------------------------------------------------------------------------------

.. class:: with-title

**Ph.D. thesis** 
    | ``Search for new physics with multi b-quark final states at LHC``
    | *to be submitted by November of 2013*
    |
    |
**M.Sc. thesis** 
    | ``Deep inelastic scattering leptons at nucleons`` 
    | ``within the  Standard model of electroweak interaction``
    | *Defended with distinction in June 2001*
    |
    |


**Feb 2011** |--| **present**   
    | DESY CMS Higgs group, Young Investigators Group  of Dr. A. Raspereza,
    | doctoral scholarships
**May 2008** |--| **Jan 2011**	   
    | DESY-Zeuthen, group of Prof. W. Lohmann
**Apr 2005** |--| **Apr 2008**	   
    | Work under INTAS Young Scientist Grant
**Aug 2001** |--| **Mar 2005**    
    | National Center of Particle and High Energy Physics,
    | Minsk, Belarus, group of Prof. N. Schumeiko,  post-graduate studies,                        
**Jul 1996** |--| **Jun 2001**    
    | Belorussian State University, Faculty of Physics, Minsk, Belarus,       
    | undergraduate studies.   Graduated with honors in June 2001.      
**Sep 1986** |--| **Jun 1996**
    | Minsk Gymnasium N3,   specialized in economics.  Graduated  in June 1996.      


-----------------------------------
Professional skills
-----------------------------------

**Operating Systems**
    | Unix/Linux (user- and kernel-level programming), 
    | Windows, Android
**Coding** 
    | Strong knowledge of C, C++, Python, Bash, TCL/Tk,  good knowledge of
    | Java, JavaScript, Perl, PHP
**Project Management**
    | SVN, GIT, CVS,  Autotools, Make
**Systems of publishing**
    | Latex, ReStructuredText
**Detector simulation** 
    | Geant4, OSCAR, ROOT(TGeant3)
**Object-oriented design: the reconstruction software for experiments**
    | CMSSW software,  software of event reconstruction in CMS developed myself
**Object-oriented design: adaptive Monte Carlo generators:**
    | simulations of parton-parton, electron-parton deep inelastic, photon-photon scattering
    | software developed myself
**Computer Algebra Systems**
    | developing modules for MATHEMATICA, modules for SymPy
**Numerical and Algebraic methods**
    | Veltman-Passarino Integrals, the system of differential  equations for the RenormGroup, 
    | dimensional regularization  in perturbative Quantum Field Theories
**Statistical analysis**
    | Frequentist/Bayesian/Likelihood-based  techniques for parameter estimation, hypothesis testing
    | ROOT, RooFit, RooStats, BAT, Minuit
**Multivariate analysis**
    | Classification/Regression analyses, probabilistic classifiers (Likelihood-based), 
    | decision trees, multi-layer perceptrons, ensemble learning algorithms, principal component analysis
    | TMVA package, software developed myself 



Languages
---------

:Belorussian: Mother Tongue
:Russian: Mother Tongue
:English: Fluent
:German: Intermediate




-----------------
Work experience
-----------------


==========      =====================           =======================================
 Period          Institute / Position             Activities
==========      =====================           =======================================
2013            convener of B-tag               Responsibility for  b-tagging in
                Physics Object Group            the CMS  High Level Trigger
                in CMS, CERN

2011-2013       DESY CMS Higgs group            The model-independent analysis of  the
                Hamburg, Germany,               `$H+b\rightarrow 3b$`
                DESY-Zeuthen,                   channel at 7+8 TeV data
                CMS/FCAL group                  recorded by CMS at LHC in 2011(2012).
                Zeuthen, Germany,               The statistical interpretation of
                PhD student of                  `$H+b\rightarrow 3b$` results in
                the supervisors                 the MSSM.  Developing and tuning MVA
                Prof. W. Lohmann and            methods for the blinding policy  of
                Dr. R. Walsh                    the Higgs search

2008-2011       DESY-Hamburg, ZEUS              Calculation of      
                group                           electroweak radiative  correction  to             
                Hamburg, Germany,               charge current DIS         
                DESY-Zeuthen,                   of `$e^{\pm}$` on protons    
                CMS group                       in ZEUS experiment.             
                Zeuthen, Germany,               Selection of `$t\bar{t}$` events  
                Researcher                      in CMS. Study of muons from  900 GeV                      
                                                and 2.3 TeV data in CMS.          
                                                Efficiency estimation            
                                                for btagging algorithms from data.                        

2005-2008       National Institute              Analysis of process like                
                for Particle and                `$pp\rightarrow\gamma\gamma
                High Energy Physics,            \rightarrow WWX1X2.$` with AQGC    
                Minsk, Belarus,                 at CMS. Membership in the RDMS CMS                       
                CERN,                           Working Group for Electroweak Physics.  
                Geneva,Switzerland,             Work under INTAS Young Scientist       
                Researcher                      Grant, no. INTAS-YS-05-112-5429

2001-2005       National Institute              The precise study of  gauge boson      
                for Particle and High           interactions in photon-photon          
                Energy Physics                  reactions in the framework             
                Minsk, Belarus,                 of SM and beyond on ee-linear          
                Junior researcher               colliders by means of two-photon       
                                                processes.                                              
                                                High accuracy analysis of              
                                                radiative corrections to WW pair       
                                                gauge boson production in               
                                                gamma-gamma collisions.                
                                                Investigation of Anomalous Quartic     
                                                Gauge  boson Coupling (AQGC)           
                                                Membership in the TESLA           
                                                Working Group                     
                                                for Electroweak Physics           
==========      =====================           =======================================


--------------------
Teaching experience
--------------------




================       =====================           ==========================================
 Period                Institute                               Subject                     
================       =====================           ==========================================
2013 (summer)          DESY,Zeuthen Germany            Advisor in DESY Summer Student Programme              

2013 (winter)          Brandenburg                     Several lectures on quantum mechanics,               
                       Technical University,           field theory       and cosmology      
                       Cottbus Germany                           

2012 (summer)          DESY, Zeuthen Germany           Advisor in DESY Summer Student Programme              

2011 (summer)          DESY, Zeuthen Germany           Advisor in DESY  Summer Student Programme                

2002                   Belorussian State               Practical lessons on  quantum mechanics               
                       University (BSU),                                            
                       Minsk Belarus   

2001                   BSU, Minsk                      Practical lessons on particle and  high            
                                                       energy physics  
================       =====================           ==========================================

-------------------
Awards and Honors
-------------------

* INTAS Young Scientist Grant, no. INTAS-YS-05-112-5429, CERN
* Awarded by "Wilhelm und Else Heraeus" foundation  in recognition for the contributions made to the  
  DPG Spring Meeting 2013 in Dresden 


--------------------------------------------------
Reference letters are available upon request from:
--------------------------------------------------
* Prof. Dr. Wolfgang Lohmann  <wolfgang.lohmann@desy.deNOSPAM>,DESY, Platanenalle 6,15738 Zeuthen, Germany 
* Dr. Roberval Walsh  <roberval.walsh@desy.deNOSPAM>,  DESY, Notketsrasse 85, 22607 Hamburg Germany


--------------------------------------------------
Additional information 
--------------------------------------------------

* My software projects: `http://igormarfin.github.io/pages/pages/research-and-software-projects.html`_
* My research statements: `http://igormarfin.github.io/pages/pages/research-statements.html`_
* A list of my publications: `http://igormarfin.github.io/pages/pages/the-list-of-publications.html`_

.. _`http://igormarfin.github.io/pages/pages/research-and-software-projects.html`: http://igormarfin.github.io/pages/pages/research-and-software-projects.html
.. _`http://igormarfin.github.io/pages/pages/research-statements.html`: http://igormarfin.github.io/pages/pages/research-statements.html
.. _`http://igormarfin.github.io/pages/pages/the-list-of-publications.html` : http://igormarfin.github.io/pages/pages/the-list-of-publications.html

.. include:: syms.rst




