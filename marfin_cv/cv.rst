
.. I can process this file to a pdf using the command : "rst2pdf -s
   cv.pdfstyle cv.rst".  I need to use the svn version otherwise it
   doesn't work.  Since I installed rst2pdf with --user option, I can
   do like this : "~/.local/bin/rst2pdf -s cv.pdfstyle cv.rst".

Guillaume Chereau, Computer Science Engineer
============================================
+------------------------------------+----------------------+------------------+
|:Address: Germany (München)         |:Born: 1982           |.. image :: id.jpg|
|:Tel: +49 (0)15778248767            |:Nationnality: French |   :scale: 50     |
|:Email: guillaume.chereau@gmail.com |                      |                  |
+------------------------------------+----------------------+------------------+


Education
---------
:2005-2006: Exchange student in the Technical University of Dresden,
  Germany.

:2000-2006: Master's Degree (Diplôme d'Ingénieur) in Computer Science
  at the National Institute of Applied Sciences (INSA) of Lyon,
  France.

:2000: Baccalauréat Scientifique : majors in Maths, Physics, Chemistry
  and Industrial Technology at Lycée du Val de Saône, Trévoux, France.


Technical skills
----------------
:Operating Systems: Unix/Linux, Windows, Symbian
:Coding: Strong knowledge of C, C++ and Python, good knowledge of
  Java, javascript, Matlab, Perl, lisp, D, Haskell, php
:Project Management: Svn, Git, Mercurial, Bazaar, Autotools, trac, CMake
:DataBase: SQL, MySQL, sqlite, posgres, tokyo cabinet
:Software: Office tools, Graphic creation, LaTeX
:Embedded Systems: openembedded, scratchbox, creation of applications
  for arm systems, Android, Maemo, Java2ME.


Languages
---------
:French: Mother Tongue
:English: Fluent
:German: Intermediate (lived one year in Germany)
:Chinese: Intermediate (lived four years in Taiwan, learned full time
  6 months in school)


Experiences
-----------

:2010 - 2011: Software engineer - Toro_ - Taipei.  Developing
  mobile phone applications using Near Field Communication
  technologies.  Mostly Java2ME and android.

:2008 - 2009: Software engineer - OpenMoko_ - Taipei.  Creating an
  opensource platform for mobile phone based on linux.

:2007 - 2008: Software Engineer - ASIAA_ (Academia Sinica Institute of
  Astronomy and Astrophysics) - Taipei.  Writing software for
  telescope control (AMiBA_ project).

:2006 (5 months): End of study project in the MPIPKS_ (Max Planck
  Institute for The Physics of Complex Systems). - Dresden, Germany.
  Writing simulation code for the Resolution of the time dependent
  Schrödinger equation.

:2004 (4 months): Traineeships student - liama_, The joint Sino-French
  Laboratory in Informatics - Beijing.  Automation and Applied
  Mathematics, in Beijing. Developing a file parsing library based on
  XML and Xquery to be used in plant growth simulation applications.

:2003 (3 months): Traineeships student at cincom_ in Lyon,
  France. Porting a c/c++ program from Windows to Linux.

.. _Toro: http://www.toro-asia.com
.. _openmoko: http://openmoko.org
.. _ASIAA: http://www.asiaa.sinica.edu.tw
.. _AMiBA: http://amiba.asiaa.sinica.edu.tw
.. _MPIPKS: http://www.mpipks-dresden.mpg.de
.. _liama: http://liama.ia.ac.cn
.. _cincom: http://www.cincom.com


Projects
--------
  * Member of the development team of stellarium_.
  * Created a few open source video games for desktop: guisterax_,
    `helvin space trip`_.
  * Made a mobile version of stellarium for the nokia N900:
    `stellarium mobile`_. awarded the first price (25000$) in Nokia
    `calling all innovators contest`_, category "Best
    application for the Nokia N900".
  * Laoshi_: an open source Chinese learning software.
  * Chatocracy_: meet new friends and talk to them with your webcam.
  * `Super Medusa`_: video game for symbian phones.

.. _`calling all innovators contest`: http://www.callingallinnovators.com/
.. _stellarium: http://www.stellarium.org
.. _`stellarium mobile`: http://stellarium-mobile.org
.. _laoshi: http://chinese-laoshi.org
.. _guisterax: http://www.dsource.org/projects/guisterax
.. _`helvin space trip`: http://sourceforge.net/projects/helvinspacetrip
.. _chatocracy: http://www.chatocracy.com
.. _`Super Medusa`: http://www.noctua-software.com/super-medusa

Personal
--------
:Hobbies: Guitar, Science Fiction, Drawing.
:Personal blog: http://charlie137.blogspot.com
:Technical blog: http://charlie137-2.blogspot.com

