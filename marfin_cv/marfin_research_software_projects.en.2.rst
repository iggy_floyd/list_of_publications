 
 
.. role:: raw-tex(raw)
    :format: latex html



.. default-role:: raw-tex



.. reStructuredText source for my english curriculum vitae.
..
.. The idea of writing my curriculum vitae using reStructuredText comes
.. from Martin Fellix Krafft, see <http://martin-krafft.net/cv/>.
..
.. The files making up this curriculum vitae are greatly inspired from
.. Martin Fellix Krafft's work.
..
.. This file is provided under the terms of the Artistic License 2.0.

.. |today| replace:: 5th September 2013





===============================================================================
                  Research and Software projects
===============================================================================
| 

Igor Marfin, physicist
============================================

+------------------------------------+------------------------+--------------------+
|:Address: Germany (Zeuthen)         |:Born: 1979             |.. image ::   id.png|
|:Tel: +49 (0)1758916099             |:Nationality: Belarus   |   :scale: 30       |
|:Email: marfin@cern.ch              |                        |   :align: right    |
+------------------------------------+------------------------+--------------------+

-----------
Research
-----------

* W gauge boson physics (phenomenology and experimental searches)

* Top quark physics (phenomenology and experimental searches)

* Higgs physics (phenomenology and experimental searches)

* Supersymmetry phenomenology

* Structure of nucleon, parton distribution functions

* Methodology of event reconstruction at future collider experiments 

* Techniques for the track reconstruction, muon reconstruction 
  and heavy flavor tagging at the collider experiments

* Techniques for calculation of high orders effects in electroweak interactions

* Techniques for Monte Carlo simulations

* Techniques of Multivariate analysis

* The statistical analysis in experiments 



------------------
Software projects
------------------


* FORTRAN based MC generator to estimate AQGC in  :math:`\gamma\gamma\rightarrow WW(Z)`

* FORTRAN based MC generator to estimate high order electroweak effects to :math:`\gamma\gamma\rightarrow WW(Z)`

* C++ based module to the CMSSW software for analysis of effects from AQGC in :math:`pp\rightarrow WWX_1X_2` process at CMS

* C++ based MC generator to simulate process of  DIS :math:`eP\rightarrow\nu X` with high order electroweak effects at HERA   

* C++ based framework for the CMSSW software to reconstruct  :math:`pp\rightarrow t\bar{t}+jets`  processes in the CMS

* C++ based framework for the CMSSW software of data-driven calibration of btagging methods in the CMS experiment

* C++ based framework for the CMSSW software of model-independent :math:`H+b\rightarrow 3b` analysis  in the CMS experiment

* C++  software based on RooStats API for statistical interpretation of the results in MSSM (SUSY)

* Python based framework for optimization  methods  of multivariate analysis. adopted to API TMVA.

.. include:: syms.rst




