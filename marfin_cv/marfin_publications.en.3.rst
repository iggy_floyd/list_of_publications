 
 
.. role:: raw-tex(raw)
    :format: latex html



.. default-role:: raw-tex



.. reStructuredText source for my english curriculum vitae.
..
.. The idea of writing my curriculum vitae using reStructuredText comes
.. from Martin Fellix Krafft, see <http://martin-krafft.net/cv/>.
..
.. The files making up this curriculum vitae are greatly inspired from
.. Martin Fellix Krafft's work.
..
.. This file is provided under the terms of the Artistic License 2.0.

.. |today| replace:: 5th September 2013





===============================================================================
		The list of publications
===============================================================================
| 

Igor Marfin, physicist
============================================

+------------------------------------+------------------------+-------------------------+
|:Address: Germany (Zeuthen)         |:Born: 1979             |.. image :: id.png       |
|:Tel: +49 (0)1758916099             |:Nationality: Belarus   |   :scale: 30            |
|:Email: marfin@cern.ch              |                        |   :align: right         |
+------------------------------------+------------------------+-------------------------+

---------------------------------
Key papers to which I contributed
---------------------------------

* The investigation of the `$\gamma\gamma \rightarrow WW$` process at the TESLA kinematics, 
  I. Marfin et al, **Nonlinear Phenomena in Complex Systems 11** (2002) 23

* Anomalous quartic boson couplings via  `$\gamma\gamma \rightarrow WW$` 
  and `$\gamma\gamma \rightarrow WWZ$` at the TESLA kinematics, 
  I. Marfin et al,  **LC NOTES, LC-PHSM-2003-085,  hep-ph/0304250** (2003)  

* High order QED effects on anomalous quartic boson vertices in gamma-gamma-colliders,
  I. Marfin et al, **Nonlinear Phenomena in Complex Systems 7** (2004) 150

* Electroweak radiative effects in WW-production at  gamma-gamma-scattering,
  I. Marfin et al, **Nonlinear Phenomena in Complex Systems 4** (2005) 40

* Investigation of Anomalous Four-Boson Couplings in Photon-Photon Collisions at High Energies,
  I. Marfin et al, **Physics of Atomic Nuclei  69** (2006) 686

* Automated calculation of radiative correction to electron-proton charged current DIS at HERA, 
  I. Marfin, **ZEUS NOTE, ZUES-09-001**

* Observation of a new particle in the search for the Standard Model Higgs boson with the ATLAS  detector at the LHC,
  ATLAS/CMS collaborations,  **Phys. Lett. B, vol. 716**\ , issue 1 (2012) 

* Search for a Higgs boson decaying into a b-quark pair  and produced in association with b quarks in proton-proton collisions at 7 TeV,
  CMS collaborations,  **Phys. Lett. B, vol. 722**\ , issue 4 (2013)  
   
------------------------------
Conference Proceedings
------------------------------


* Quartic boson interaction in gamma-gamma-colliders,
   I. Marfin, V. Mossolov, T. Shishkina,  *Proceedings of the International Linear Collider*,
   4th ECFA/DESY Workshop, Amsterdam,   April 03 - April 08 2004, **DESY-PROC-2004-01, DESY-04-123**
   
* Study of Boson Vertices at Photon Colliders in the Standard Model and Beyond,
   I. Marfin et al,  *Proceedings of the 23rd International conference ``Physics in Collisions*
   Zeuthen, June 26 - June 28 2003, **eConf C030626, GERMANY DESY-PROC-2003-2**

* Contribution of radiative corrections to anomalous     constant in gamma-gamma -> WW
   I. Marfin, V. Mossolov, T. Shishkina, Proceeding of the School-Seminar "The actual Problems  of Microworld Physics",
   Gomel, 01-10 July 2003, **hep-ph/0305153**

* Two-photon process for  investigation of  the bosonic AQCG in the high energy pp-collisions,
   I. Marfin, V. Mossolov, T. Shishkina   Proceeding of the School-Seminar "The actual Problems  of Microworld Physics",
   Gomel, 10-20 July 2005 

* Quartic gauge boson couplings study,
   I. Marfin,  *Proceedings of 10th RDMS CMS Conference*,
   PNPI St. Peterburg, September 2005, **CMS 2005-013 477**
   
* Anomalous Quartic Gauge boson Couplings study in CMS
   I. Marfin, V. Makarenko, *Proceedings of 11th RDMS CMS Conference*,
   INRNE Varna, September 2006
   
* Search for Higgs boson production in association with b quarks at CMS
   J. Behr, W. Lohmann, R. Mankel, I. Marfin, A. Raspereza, A. Spiridonov, R. Walsh,
   *Proceedings of 32nd International Symposium on Physics in Collision (PIC 2012)*,
   Slovak Acad. Sci, Strbske Pleso,  September  2012,
   **Slovak Acad. Sci., Inst. Exp. Phys.** (2013)



.. include:: syms.rst




