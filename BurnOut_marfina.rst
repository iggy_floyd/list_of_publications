================================================================
 Burn-Out
================================================================




:Autor: Julia Marfina
:Kontakt: yulia.marfina@gmail.com
:Datum: Oct 28, 2015, 9:28:51 AM
:Version: 1
:Link zum Herunterladen: https://bitbucket.org/iggy_floyd/burn_out_psyhology

.. figure:: figs/burnout_1708025.jpg
   :align: center
   :width: 770px
   :height: 520px
   :scale: 100
   :alt: Burn-Out




.. admonition:: Zusammenfassung

    Für den Begriff Burn-Out existieren unterschiedliche Bezeichnungen und Schreibweisen: 
    Burn-Out, Burnout, Burnoutsyndrom, BurnOut-Syndrom. 
    Der Begriff kommt aus dem Englischen und bedeutet "ausbrennen" oder "ausgebrannt sein". 
    Gemeint ist ein berufsbezogenes Erschöpfungssyndrom.



.. meta::
   :keywords: Burn-Out,Burnout,Erschöpfungssyndrom.
   :description lang=en: A German assey on the Burnout topic in the psychology.





.. contents:: Inhaltsverzeichnis 




.. section-numbering::








.. raw:: pdf

   PageBreak cutePage




----------------------------------------------------------------
 Was  ist  Burn-Out?
----------------------------------------------------------------

:math:`\frac{1}{2}`

Bisher ist das Burn-Out-Syndrom medizinisch nur unzureichend erforscht. 
So existiert bisher keine medizinisch verbindliche Definition noch ein allgemeingültiges Instrument für die Diagnostik. 
Durch diese Unsicherheit wird der Begriff sehr inflationär benutzt und ist eine häufige "Modediagnose".
 
Besonders anfällig für das Burn-Out-Syndrom sind Menschen in sozialen Berufen wie 
beispielsweise Gesundheits- und Kranken-, Alten-, Heilerziehungs-, Kinderpfleger, Erzieher, Lehrer, Therapeuten. 
Es betrifft also überwiegend die Berufe, die eng mit Menschen zusammenarbeiten, wobei die Übernahme der Verantwortung sehr einseitig ist. 
Es können  aber auch zum Beispiel Manager oder Arbeitslose betroffen sein.
 
Burn-Out ist eine schwerwiegende Stresserkrankung, die aber nicht von einem einzigen Stressfaktor 
verursacht wird, sondern durch eine Kombination von arbeitsbedingten und persönlichen Stressfaktoren. 
Man "brennt" nicht von einem Tag zum anderen "aus", sondern es ist ein schleichender Prozess, der in mehreren Stufen oder Phasen abläuft mit typischen Symptomen. 



Definition Burn-Out
=======================

Burn-Out  wurde  1974  vom  US amerikanischen  Psychoanalytiker  J.  Freudenberger  beschrieben.  Er  
bezeichnete  mit  Burnout  den  psychischen  und  physischen  Abbau  von  Mitarbeitern  sozialer  
Hilfsorganisationen,  die  in  therapeutischen  Wohngemeinschaften,  Frauenhäusern  oder  
Kriseninterventionszentren  gearbeitet  haben.

Hinzu  kam  die  Erscheinung  eines  vollständigen  Motivationsverlustes.  Später  wurde  der  Begriff  auch  auf  andere  
Berufsgruppen  ausgeweitet und  erhielt  in  den  80er  Jahren  auch  in  Deutschland  Aufmerksamkeit  in  den  Medien  und  in  Fachkreisen.

Freudenberger  (1974)  definiert  Burn-Out  als ::

	„Nachlassen  bzw.  Schwinden  von  Kräften  oder  Erschöpfung  
	durch  übermäßige  Beanspruchung  der  eigenen  Energie,  
	Kräfte oder  Ressourcen“.  


Cherniss  (1980)  definiert  Burn-Out  als  ::

	„das Resultat eines  ...Prozesses,  der  sich  aus  Arbeitsbelastungen,  
	Stress und  psychologischer  Anpassung zusammensetzt,  in  welchem  ein  
	ursprünglich  engagierter  Professioneller  sich  als  
	Reaktion  auf  die  in  der  Arbeit  erfahrenen Stressoren  und  
	den  erlebten  Stress von  der  Arbeit  zurückzieht“



Burisch  (2006)  definiert  Burn-Out  psychologisch-metaphorisch  als  ::

	„eine  langandauernde  zu  hohe  Energieabgabe  für  zu  geringe  
	Wirkung  bei  ungenügendem  Energienachschub."



    
----------------------------------------------------------------
Burn-Out: Phasenmodelle (Fortsetzung)
----------------------------------------------------------------

Maslach (1982) [#Maslach]_:

.. [#Maslach] Maslach, C. (1982): Burnout - The Cost of Caring. Engelwood Cliffs, NJ: Prentice-Hall. 

1. **Emotionale und physische Erschöpfung**

   Müdigkeit schon beim Gedanken an die Arbeit, Schlafstörungen, Anfälligkeit für Erkältungen und Schmerzen.

2. **Depersonalisation**

   negative Gefühle und zynische Einstellung gegenüber anderen (Kollegen, Patienten, Klienten, etc.), 
   Schuldgefühle, sozialer Rückzug, Reduzierung der Arbeit aufs Notwendigste.

3. **Reduzierte Leistungsfähigkeit**

   Tendenz, die eigene Arbeit negativ zu bewerten, mangelndes Selbstwertgefühl, Widerwillen gegen alles







.. raw:: pdf

   PageBreak coverPage



----------------------------------------------------------------
References
----------------------------------------------------------------


    
.. target-notes::


