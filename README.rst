================================================================
The list of my publications
================================================================





:Author: Igor Marfin
:Contact: igor.marfin@unister.de
:Organization: private
:Date: Oct 29, 2015, 9:28:51 AM
:Status: draft
:Version: 1
:Copyright: This document has been placed in the public domain. You
            may do with it as you wish. You may copy, modify,
            redistribute, reattribute, sell, buy, rent, lease,
            destroy, or improve it, quote it at length, excerpt,
            incorporate, collate, fold, staple, or mutilate it, or do
            anything else to it that your or anyone else's heart
            desires.

.. admonition:: Dedication

    For physicists

.. admonition:: Abstract

    This is a repository of the list with my publications, my CV in English and German.

.. meta::
   :keywords: german, english, cv, list of publications
   :description lang=en:  The list with my publications




.. contents:: Table of Contents







----------------------------------
Installation of the package
----------------------------------


.. Important::
  
    The project relies on the presence of ``Autotools`` in the system.
    Before  to proceed further, please, install them: ``sudo apt-get install autotools-dev``.

Simply, clone the project 

.. code-block:: 

    git clone https://iggy_floyd@bitbucket.org/iggy_floyd/list_of_publications.git

and test a configuration of your system that you have all components installed:
    
* python 


In order to do this, you can just run the default rule of  ``Makefile`` via the 
command in the shell:

.. code-block:: 
    
    make

    
